{-|
    Module      : Lib
    Description : Tweede checkpoint voor V2DeP: cellulaire automata
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we aan de slag met 1D cellulaire automata [<https://mathworld.wolfram.com/Rule30.html>].
-}

-- TODO add voorbeelden
-- TODO add interne documentatie

module Lib where

import Data.Maybe (catMaybes) -- Niet gebruikt, maar deze kan van pas komen...
import Data.List (unfoldr)
import Data.Tuple (swap)

-- Om de state van een cellulair automaton bij te houden bouwen we eerst een set functies rond een `FocusList` type. Dit type representeert een 1-dimensionale lijst, met een
-- enkel element dat "in focus" is. Het is hierdoor mogelijk snel en makkelijk een enkele cel en de cellen eromheen te bereiken.

-- * FocusList

{- | The focussed list [0 1 2 ⟨3⟩ 4 5] is represented as @FocusList [3,4,5] [2,1,0]@. The first element (head) of the first list is focussed, and is easily and cheaply accessible.
 -   The items before the focus are placed in the backwards list in reverse order, so that we can easily move the focus by removing the focus-element from one list and prepending
 -   it to the other.
-}
data FocusList a = FocusList { forward :: [a]
                             , backward :: [a]
                             }
  deriving Show

-- De instance-declaraties mag je voor nu negeren.
instance Functor FocusList where
  fmap = mapFocusList

-- Enkele voorbeelden om je functies mee te testen:
intVoorbeeld :: FocusList Int
intVoorbeeld = FocusList [3,4,5] [2,1,0]

stringVoorbeeld :: FocusList String
stringVoorbeeld = FocusList ["3","4","5"] ["2","1","0"]

-- Schrijf en documenteer een functie die een focus-list omzet in een gewone lijst. Het resultaat bevat geen focus-informatie meer, maar moet wel op de juiste volgorde staan.
-- toList intVoorbeeld ~> [0,1,2,3,4,5]
{- |The function toList takes a FocusList and returns a normal list.
  There is 1 argument which is a FocusList-}
toList :: FocusList a -> [a]
toList (FocusList fw bw) = (reverse bw) ++ fw

-- Schrijf en documenteer een functie die een gewone lijst omzet in een focus-list. Omdat een gewone lijst geen focus heeft moeten we deze kiezen; dit is altijd het eerste element.
{-|The function fromList takes a normal list and returns a focus list with the first element of the list in focus.
  There is 1 argument wich is a list.
-}
fromList :: [a] -> FocusList a
fromList a = (FocusList a [])

{- | The function goLeft takes a FocusList and moves the focus one to the left.
   This function takes 1 argument which is a FocusList.
-}
goLeft :: FocusList a -> FocusList a
goLeft (FocusList fw (f:bw)) = FocusList (f:fw) bw

-- Schrijf en documenteer zelf een functie goRight die de focuslist een plaats naar rechts opschuift.
{-|The function goRight takes a FocusList and moves the focus one to the right.
  This function takes 1 argument which is a FocusList.
-}
goRight :: FocusList a -> FocusList a
goRight (FocusList (fh:ft) bw) = FocusList ft (fh:bw)

-- Schrijf en documenteer een functie leftMost die de focus geheel naar links opschuift.
{-|The function leftMost moves the focus to the most left element (if the FocusList is seen as a list).
  This function takes 1 argument which is a FocusList.-}
leftMost :: FocusList a -> FocusList a
leftMost (FocusList fw bw) = (FocusList ((reverse bw)++fw) [])

-- Schrijf en documenteer een functie rightMost die de focus geheel naar rechts opschuift.
{-|The function rightMost moves the focus to the most right element (if the FocusList is seen as a list).
  This function takes 1 argument which is a FocusList.-}
rightMost :: FocusList a -> FocusList a
rightMost (FocusList fw bw) = (FocusList [head (reverse fw)] (tail(reverse fw)++bw))

-- De functies goLeft en goRight gaan er impliciet vanuit dat er links respectievelijk rechts een cell gedefinieerd is. De aanroep `goLeft $ fromList [1,2,3]` zal echter crashen
-- omdat er in een lege lijst gezocht wordt: er is niets links. Dit is voor onze toepassing niet handig, omdat we bijvoorbeeld ook de context links van het eerste vakje nodig
-- hebben om de nieuwe waarde van dat vakje te bepalen (en dito voor het laatste vakje rechts).

-- Schrijf en documenteer de functies totalLeft en totalRight die de focus naar links respectievelijk rechts opschuift; als er links/rechts geen vakje meer is, dan wordt een
-- lege (dode) cel teruggeven. Hiervoor gebruik je de waarde `mempty`, waar we met een later college nog op in zullen gaan. Kort gezegd zorgt dit ervoor dat de FocusList ook
-- op andere types blijft werken - je kan dit testen door totalLeft/totalRight herhaaldelijk op de `voorbeeldString` aan te roepen, waar een leeg vakje een lege string zal zijn.

-- [⟨░⟩, ▓, ▓, ▓, ▓, ░]  ⤚goLeft→ [⟨░⟩, ░, ▓, ▓, ▓, ▓, ░]

{-|The function totalLeft moves the focus one to the left, if there isn't something there it inserts a mempty object.
  This function takes 1 argument which is a FocusList.-}
totalLeft :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalLeft (FocusList fw []) = FocusList (mempty:fw) []
totalLeft (FocusList fw (bh:bt)) = goLeft (FocusList fw (bh:bt))

{-|The function totalRight moves the focus one to the right, if there isn't something there it inserts a mempty object.
  This function takes 1 argument which is a FocusList.-}
totalRight :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalRight (FocusList [] bw) = FocusList [mempty] bw
totalRight (FocusList (fh:ft) bw) = goRight (FocusList (fh:ft) bw)



-- In de colleges hebben we kennis gemaakt met een aantal hogere-orde functies zoals `map`, `zipWith` en `fold[r/l]`. Hier zullen we equivalenten voor de FocusList opstellen.
-- De functies mapFocusList werkt zoals je zou verwachten: de functie wordt op ieder element toegepast, voor, op en na de focus. Je mag hier gewoon map voor gebruiken
{-|The function mapFocusList does the same as the normal map function, only now on the FocusList.
  It basically does the map function on both the lists in the FocusList.
  This function takes 2 arguments: a  function and a FocusList.-}
mapFocusList :: (a -> b) -> FocusList a -> FocusList b
mapFocusList f (FocusList fw bw) = (FocusList (map f fw) (map f bw))

-- De functie zipFocusList zorgt ervoor dat ieder paar elementen uit de FocusLists als volgt met elkaar gecombineerd wordt:
-- [1, 2, ⟨3⟩,  4, 5]
-- [  -1, ⟨1⟩, -1, 1, -1]
--------------------------- (*)
-- [  -2, ⟨3⟩, -4, 5    ]

-- Oftewel: de megegeven functie wordt aangeroepen op de twee focus-elementen, met als resultaat het nieuwe focus-element. Daarnaast wordt de functie paarsgewijs naar
-- links/rechts doorgevoerd, waarbij gestopt wordt zodra een van beide uiteinden leeg is. Dit laatste is net als bij de gewone zipWith, die je hier ook voor mag gebruiken.

{-|The function zipFocusListWith zips two FocusLists into one.
  It does this by zipping the two lists in each focus list with the corresponding list in the other FocusList.
  This function takes 3 arguments: a function and 2 FocusLists.-}
zipFocusListWith :: (a -> b -> c) -> FocusList a -> FocusList b -> FocusList c
zipFocusListWith f (FocusList fw1 bw1) (FocusList fw2 bw2) = (FocusList (zipWith (f) fw1 fw2) (zipWith (f) bw1 bw2))

-- Het folden van een FocusList vergt de meeste toelichting: waar we met een normale lijst met een left fold en een right fold te maken hebben, moeten we hier vanuit de focus werken.
-- Vanuit de focus worden de elementen van rechts steeds gecombineerd tot een nieuw element, vanuit het element voor de focus gebeurt hetzelfde vanuit links. De twee resultaten van
-- beide sublijsten (begin tot aan focus, focus tot en met eind) worden vervolgens nog een keer met de meegegeven functie gecombineerd. Hieronder een paar voorbeelden:

-- foldFocusList (*) [0, 1, 2, ⟨3⟩, 4, 5] = (0 * (1 * 2)) * ((3 * 4) * 5)

-- foldFocusList (-) [0, 1, 2, ⟨3⟩, 4, 5] = (0 - (1 - 2)) - ((3 - 4) - 5)
-- foldFocusList (-) [0, 1, 2, ⟨3⟩, 4, 5] = (0 - (-1)) - ((-1) - 5)
-- foldFocusList (-) [0, 1, 2, ⟨3⟩, 4, 5] = 1 - (-6)
-- foldFocusList (-) [0, 1, 2, ⟨3⟩, 4, 5] = 7

-- Je kunt `testFold` gebruiken om je functie te testen. Denk eraan dat de backwards lijst achterstevoren staat, en waarschijnlijk omgekeerd moet worden.

{-|The function foldFocusList folds the a FocusList to one element.
  It does this by folding the two lists to two elements and then folding those two elements.
  This function takes 2 arguments: a function and a FocusList.-}
foldFocusList :: (a -> a -> a) -> FocusList a -> a
foldFocusList f (FocusList fw bw) = f (foldr1 f bw_twee) (foldl1 f fw) -- Gebruik foldr1 om het laaste element als default waarde te gebruiken.
   where bw_twee = reverse bw

-- | Test function for the behaviour of foldFocusList.
testFold :: Bool
testFold = and [ foldFocusList (+) intVoorbeeld     == 15
               , foldFocusList (-) intVoorbeeld     == 7
               , foldFocusList (++) stringVoorbeeld == "012345"
               ]

-- * Cells and Automata

-- Nu we een redelijk complete FocusList hebben kunnen we gaan kijken naar daadwerkelijke celulaire automata, te beginnen met de Cell.

-- | A cell can be either on or off, dead or alive. What basic type could we have used instead? Why would we choose to roll our own equivalent datatype?
data Cell = Alive | Dead deriving (Show, Eq)

-- De instance-declaraties mag je voor nu negeren.
instance Semigroup Cell where
  Dead <> x = x
  Alive <> x = Alive

instance Monoid Cell where
  mempty = Dead

-- | The state of our cellular automaton is represented as a FocusList of Cells.
type Automaton = FocusList Cell

-- | Start state, per default, is a single live cell.
start :: Automaton
start = FocusList [Alive] []

-- | Alternative start state with 5 alive cells, for shrinking rules.
fiveAlive :: Automaton
fiveAlive = fromList $ replicate 5 Alive

-- | A rule [<https://mathworld.wolfram.com/Rule30.html>] is a mapping from each possible combination of three adjacent cells to the associated "next state".
type Context = [Cell]
type Rule = Context -> Cell

-- * Rule Iteration

-- Schrijf en documenteer een functie safeHead die het eerste item van een lijst geeft; als de lijst leeg is wordt een meegegeven default values teruggegeven.
{-|The function safeHead takes the head of a list, if there is no head available a default value is returned.
  This function takes 2 arguments: a default value and a list.-}
safeHead :: a        -- ^ Default value
         -> [a]      -- ^ Source list
         -> a
safeHead dv (x:xs) = x
safeHead dv x = dv -- Er is geen head, return default waarde

-- Schrijf en documenteer een functie takeAtLeast die werkt als `take`, maar met een extra argument. Als de lijst lang genoeg is, bijvoorbeeld
-- `takeAtLeast 3 "0" ["1","2","3","4","5"]` dan werkt de functie hetzelfde als `take` en worden de eerste `n` (hier 3) elementen teruggegeven.
-- Als dat niet zo is dan worden zoveel mogelijk elementen teruggegeven, en wordt de lijst daarna tot de gevraagde lengte aangevuld met een
-- meegegeven default-waarde: `takeAtLeast 3 "0" ["1"] ~> ["1", "0", "0"]`.
{-|The function takeAtLeast takes the requests amount of alements from a list if the lists has too little elements, a default value is added to the returned list.
  This function takes 3 arguments: an Int, a default value and a list.-}
takeAtLeast :: Int   -- ^ Number of items to take
            -> a     -- ^ Default value added to the right as padding
            -> [a]   -- ^ Source list
            -> [a]
takeAtLeast a dv x = if a <= length x
                      then take a x -- Lijst is lang genoeg
                      else x ++ (take (a-(length x)) (repeat dv)) -- Lijst is niet lang genoeg


-- Schrijf en documenteer een functie context die met behulp van takeAtLeast de context van de focus-cel in een Automaton teruggeeft. Niet-gedefinieerde cellen zijn per definitie Dead.
{-|The function context takes the focus element and the element before and after it.
  This function takes 1 argument which is an Automaton.-}
context :: Automaton -> Context
context (FocusList fw bw) = safeHead Dead bw : takeAtLeast 2 Dead fw

{-
context [0,1,2,<3>,4,5] =
context [3,4,5] [2,1,0] = [2,3,4]

-}

-- Schrijf en documenteer een functie expand die een Automaton uitbreid met een dode cel aan beide uiteindes. We doen voor deze simulatie de aanname dat de "known universe"
-- iedere ronde met 1 uitbreid naar zowel links als rechts.
{-|The function expand expands an Automaton with 2 dead cells, one on the right and one on the left.
  This function takes 1 argument which is an Automaton-}
expand :: Automaton -> Automaton
expand (FocusList fw bw) = (FocusList (fw++[Dead]) (Dead:bw))

-- | A sequence of Automaton-states over time is called a TimeSeries.
type TimeSeries = [Automaton]

-- Voorzie onderstaande functie van interne documentatie, d.w.z. zoek uit en beschrijf hoe de recursie verloopt. Zou deze functie makkelijk te schrijven zijn met behulp van
-- de hogere-orde functies die we in de les hebben gezien? Waarom wel/niet?

-- | Iterate a given rule @n@ times, given a start state. The result will be a sequence of states from start to @n@.
iterateRule :: Rule          -- ^ The rule to apply
            -> Int           -- ^ How many times to apply the rule
            -> Automaton     -- ^ The initial state
            -> TimeSeries
iterateRule r 0 s = [s] -- Geef de Automaton terug
iterateRule r n s = s : iterateRule r (pred n) (fromList $ applyRule $ leftMost $ expand $ s)
  where applyRule :: Automaton -> Context
        applyRule (FocusList [] bw) = [] -- Geef een lege lijst als de Automaton leeg is.
        applyRule z = r (context z) : applyRule (goRight z) -- Gaat staps gewijs door de Automaton heen om the kijken of the volgende cell dood of levend is.

-- | Convert a time-series of Automaton-states to a printable string.
showPyramid :: TimeSeries -> String
showPyramid zs = unlines $ zipWith showFocusList zs $ reverse [0..div (pred w) 2]
  where w = length $ toList $ last zs :: Int
        showFocusList :: Automaton -> Int -> String
        showFocusList z p = replicate p ' ' <> concatMap showCell (toList z)
        showCell :: Cell -> String
        showCell Dead  = "░"
        showCell Alive = "▓"

-- Vul de functie rule30 aan met de andere 7 gevallen. Je mag de voorbeeldregel aanpassen/verwijderen om dit in minder regels code te doen. De underscore _ is je vriend.
{-|The function rule30 defines all the rules for when a cell is dead of alive for the rule 30.-}
rule30 :: Rule
rule30 [Dead, Dead, Dead] = Dead
rule30 [Dead, Dead, Alive] = Alive
rule30 [Dead, Alive, Dead] = Alive
rule30 [Dead, Alive, Alive] = Alive
rule30 [Alive, Dead, Dead] = Alive
rule30 [Alive, Dead, Alive] = Dead
rule30 [Alive, Alive, Dead] = Dead
rule30 [Alive, Alive, Alive] = Dead


-- ...

-- Je kan je rule-30 functie in GHCi testen met het volgende commando:
-- putStrLn . showPyramid . iterateRule rule30 15 $ start

-- De verwachte uitvoer is dan:
{-             ▓
              ▓▓▓
             ▓▓░░▓
            ▓▓░▓▓▓▓
           ▓▓░░▓░░░▓
          ▓▓░▓▓▓▓░▓▓▓
         ▓▓░░▓░░░░▓░░▓
        ▓▓░▓▓▓▓░░▓▓▓▓▓▓
       ▓▓░░▓░░░▓▓▓░░░░░▓
      ▓▓░▓▓▓▓░▓▓░░▓░░░▓▓▓
     ▓▓░░▓░░░░▓░▓▓▓▓░▓▓░░▓
    ▓▓░▓▓▓▓░░▓▓░▓░░░░▓░▓▓▓▓
   ▓▓░░▓░░░▓▓▓░░▓▓░░▓▓░▓░░░▓
  ▓▓░▓▓▓▓░▓▓░░▓▓▓░▓▓▓░░▓▓░▓▓▓
 ▓▓░░▓░░░░▓░▓▓▓░░░▓░░▓▓▓░░▓░░▓
▓▓░▓▓▓▓░░▓▓░▓░░▓░▓▓▓▓▓░░▓▓▓▓▓▓▓ -}

-- * Rule Generation

-- Er bestaan 256 regels, die we niet allemaal met de hand gaan uitprogrammeren op bovenstaande manier. Zoals op de genoemde pagina te zien is heeft het nummer te maken met binaire
-- codering. De toestand van een cel hangt af van de toestand van 3 cellen in de vorige ronde: de cel zelf en diens beide buren (de context). Er zijn 8 mogelijke combinaties
-- van 3 van dit soort cellen. Afhankelijke van het nummer dat een regel heeft mapt iedere combinatie naar een levende of dode cel.

-- Definieer allereerst een constante `inputs` die alle 8 mogelijke contexts weergeeft: [Alive,Alive,Alive], [Alive,Alive,Dead], etc.
-- Je mag dit met de hand uitschrijven, maar voor extra punten kun je ook een lijst-comprehensie of andere slimme functie verzinnen.

{-|The function inputs gives all the different possible combinations in a list.-}
inputs :: [Context]
inputs = [[Alive,Alive,Alive], [Alive,Alive,Dead], [Alive,Dead,Alive], [Alive,Dead,Dead], [Dead,Alive,Alive], [Dead,Alive,Dead], [Dead,Dead,Alive], [Dead,Dead,Dead]]

-- | If the given predicate applies to the given value, return Just the given value; in all other cases, return Nothing.
guard :: (a -> Bool) -> a -> Maybe a
guard p x | p x = Just x
          | otherwise = Nothing

-- Deze functie converteert een Int-getal naar een binaire representatie [Bool]. Zoek de definitie van `unfoldr` op met Hoogle en `guard` in Utility.hs; `toEnum` converteert
-- een Int naar een ander type, in dit geval 0->False en 1->True voor Bool. Met deze kennis, probeer te achterhalen hoe de binary-functie werkt en documenteer dit met Haddock.
{-|The function binary takes an number from 0 to 256 and turns it to an binary value where 0=False and 1=True.
  This function takes 1 argument which is an Int.-}
binary :: Int -> [Bool]
binary = map toEnum . reverse . take 8 . (++ repeat 0)
       . unfoldr (guard (/= (0,0)) . swap . flip divMod 2)

-- Schrijf en documenteer een functie mask die, gegeven een lijst Booleans en een lijst elementen alleen de elementen laat staan die (qua positie) overeenkomen met een True.
-- Je kan hiervoor zipWith en Maybe gebruiken (check `catMaybes` in Data.Maybe) of de recursie met de hand uitvoeren.
{-|The function mask takes a list with booleans ans another and returns a list where the the True value indexes where the same as the other list.
  This function takes 2 argument: a list with booleans and a list.-}
mask :: [Bool] -> [a] -> [a]
mask (bh:bt) (lh:lt) = if bh == True
                          then lh : mask bt lt
                          else mask bt lt
mask b _ = []

-- Combineer `mask` en `binary` met de library functie `elem` en de eerder geschreven `inputs` tot een rule functie. Denk eraan dat het type Rule een short-hand is voor een
-- functie-type, dus dat je met 2 argumenten te maken hebt. De Int staat hierbij voor het nummer van de regel, dat je eerst naar binair moet omrekenen; de Context `input` is
-- waarnaar je kijkt om te zien of het resultaat met de gevraagde regel Dead or Alive is. Definieer met `where` subset van `inputs` die tot een levende danwel dode cel leiden.
-- Vergeet niet je functie te documenteren.
{-|The function Rule takes a number and returns the rule, so when a cell dies and when it lives.
  This function takes 1 argument which is an Int.-}
rule :: Int -> Rule
--rule n input = error (show ( mask(binary n) inputs))
--rule n input = error show (elem input inputs where inputs = mask(binary n) inputs)
rule n input
    | elem input trueFalseRules = Alive
    | otherwise = Dead
    where trueFalseRules = mask(binary n) inputs

{- Je kan je rule-functie in GHCi testen met variaties op het volgende commando:

   putStrLn . showPyramid . iterateRule (rule 18) 15 $ start

                  ▓
                 ▓░▓
                ▓░░░▓
               ▓░▓░▓░▓
              ▓░░░░░░░▓
             ▓░▓░░░░░▓░▓
            ▓░░░▓░░░▓░░░▓
           ▓░▓░▓░▓░▓░▓░▓░▓
          ▓░░░░░░░░░░░░░░░▓
         ▓░▓░░░░░░░░░░░░░▓░▓
        ▓░░░▓░░░░░░░░░░░▓░░░▓
       ▓░▓░▓░▓░░░░░░░░░▓░▓░▓░▓
      ▓░░░░░░░▓░░░░░░░▓░░░░░░░▓
     ▓░▓░░░░░▓░▓░░░░░▓░▓░░░░░▓░▓
    ▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓
   ▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓

   putStrLn . showPyramid . iterateRule (rule 128) 10 $ fiveAlive

               ▓▓▓▓▓
              ░░▓▓▓░░
             ░░░░▓░░░░
            ░░░░░░░░░░░
           ░░░░░░░░░░░░░
          ░░░░░░░░░░░░░░░
         ░░░░░░░░░░░░░░░░░
        ░░░░░░░░░░░░░░░░░░░
       ░░░░░░░░░░░░░░░░░░░░░
      ░░░░░░░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░░░░░░░░░░

   Als het goed is zal `stack run` nu ook werken met de voorgeschreven main functie; experimenteer met verschillende parameters en zie of dit werkt.
-}
