module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- Schrijf een functie die de som van een lijst getallen berekent, net als de `product` functie uit de les.
-- |De functie ex1 rekent de som uit van alle getalen uit een gegeven lijst.
-- Er wordt 1 argument verwacht van het type [Int].
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 (h:t) = h + (ex1 t)

-- Schrijf een functie die alle elementen van een lijst met 1 ophoogt; bijvoorbeeld [1,2,3] -> [2,3,4]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- |De functie ex2 verhoogt ieder getal in de lijst met 1.
-- Er wordt 1 argument verwacht van het type [Int].
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 list = (head list + 1) : (ex2 (tail list))


-- Schrijf een functie die alle elementen van een lijst met -1 vermenigvuldigt; bijvoorbeeld [1,-2,3] -> [-1,2,-3]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- |De functie ex3 vermenigvuldigt ieder getal in een lijst met -1.
-- Er wordt 1 argument verwacht van het type [Int].
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 list = (head list * (-1)) : (ex3 (tail list))

-- Schrijf een functie die twee lijsten aan elkaar plakt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1,2,3,4,5,6]. Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
-- |De functie ex4 plakt 2 lijsten aan elkaar.
-- Er worden 2 argument verwacht van het type [Int].
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] y = y
ex4 x y = head x : (ex4 (tail x) y)


-- Schrijf een functie die twee lijsten paarsgewijs bij elkaar optelt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1+4, 2+5, 3+6] oftewel [5,7,9]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- |De functie ex5 telt ieder getal uit een lijst bij elkaar op.
-- Er worden 2 argument verwacht van het type [Int].
ex5 :: [Int] -> [Int] -> [Int]
ex5 [] y = y
ex5 x y = (head x + head y) : (ex5 (tail x) (tail y))



-- Schrijf een functie die twee lijsten paarsgewijs met elkaar vermenigvuldigt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1*4, 2*5, 3*6] oftewel [4,10,18]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- |De functie ex6 vermenigvuldigt ieder getal uit een lijst met elkaar.
-- Er worden 2 argument verwacht van het type [Int].
ex6 :: [Int] -> [Int] -> [Int]
ex6 [] y = y
ex6 x y = (head x * head y) : (ex6 (tail x) (tail y))

-- Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig prodct uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
-- |De functie ex7 vermenigvuldigt ieder getal met elkaar en neemt daar de som van.
-- Er worden 2 argument verwacht van het type [Int].
ex7 :: [Int] -> [Int] -> Int
ex7 [] y = 0
ex7 x y = (head x * head y) + (ex7 (tail x) (tail y))