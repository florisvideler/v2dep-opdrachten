safeDiv :: Int -> Int -> Maybe Int
safeDiv _ 0 = Nothing
safeDiv a b = Just (div a b)


data DictEntry k v = KeyVal k v deriving Show

instance Functor (DictEntry k) where
  fmap f (KeyVal k v) = (KeyVal k (f v))


functies :: [Int -> Int]
functies = [(+1), (*2)]

data OneOrTwo a = Single a | Couple a a deriving Show

instance Functor OneOrTwo where
  fmap f (Single a) = Single $ f a
  fmap f (Couple a1 a2) = Couple (f a1) (f a2)

instance Applicative OneOrTwo where
  pure (Single a) = Just a
  pure (Couple a a) = Just a a
  appF <*> appW =

data DictEntry k v = KeyVal k v deriving Show

instance Functor (DictEntry k) where
  fmap f (KeyVal k v) = KeyVal k $ f v

--instance Applicative (DictEntry k) where
  -- TODO: KAN NIET, WAAROM