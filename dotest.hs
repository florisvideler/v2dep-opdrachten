data Colour = R | G | B | C | Y deriving (Show, Eq)

meng :: Colour -> Colour -> Either String Colour
meng R R = Right R
meng R G = Right Y
meng R B = Left "Paars bestaat niet"
meng R _ = Left "Te moeilijk"
meng G R = Right Y
meng G G = Right G
meng G B = Right C
meng G _ = Left "Te moeilijk"
meng B R = Left "Paars bestaat niet"
meng B G = Right C
meng B B = Right B
meng B _ = Left "Te moeilijk"
meng c1 c2 | c1 == c2          = Right c1
           | c2 `elem` [R,G,B] = meng c2 c1
           | otherwise         = Left "Veel te moeilijk"


meng3 :: Colour -> Colour -> Colour -> Either String Colour
meng3 c1 c2 c3 = do clr <- meng c1 c2
                    finalclr <- meng clr c3
                    return finalclr