type Stand = (String, Integer, Integer, String)

tussenstand :: Stand
tussenstand = ("Emmen", 3, 4, "VVV-Venlo")

toonStand :: Stand -> String  -- Helperfunctie, om de stand te laten zien
toonStand (tt, ts, us, ut) = concat [tt, "  ", show ts, "-", show us, "  ", ut]

doelpunt :: Bool -> Stand -> (String, Stand)
doelpunt thuisofuit (tt, ts, us, ut) =
  let ts' = if thuisofuit then ts+1 else ts
      us' = if thuisofuit then us else us+1
      leider = if ts' > us' then tt else (if ts' < us' then ut else "Gelijkspel")
  in (leider, (tt, ts', us', ut))

thuisDoelpunt :: Stand -> (String, Stand)
thuisDoelpunt = doelpunt True

uitDoelpunt :: Stand -> (String, Stand)
uitDoelpunt = doelpunt False


fcg_psv_tussenstand = ("FCG", 0, 0, "PSV")
-- fcg_psv :: ???
--fcg_psv s =
--  let (l, s') = uitDoelpunt s
--      (l', s'') uitDoelpunt s'
--  in thuisDoelpunt s''
