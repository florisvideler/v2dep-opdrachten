-- Functies voor State (niet aankomen)

import Control.Monad (ap)
import Control.Applicative (Applicative(..), liftA)

newtype State s a = State {runState :: s -> (a, s)}

instance Functor (State s) where
    fmap = liftA

instance Applicative (State s) where
    {-# INLINE pure #-}
    pure x = State $ \ s -> (x, s)
    (<*>) = ap

instance Monad (State s) where
    {-# INLINE return #-}
    {-# INLINE (>>=) #-}
    return = pure
    m >>= k = State $ \ s -> case runState m s of
        (x, s') -> runState (k x) s'

-- Functies voor ons programma

type Stand = (String, Integer, Integer, String)

tussenstand :: Stand
tussenstand = ("Emmen", 3, 4, "VVV-Venlo")

toonStand :: Stand -> String  -- Helperfunctie, om de stand te laten zien
toonStand (tt, ts, us, ut) = concat [tt, "  ", show ts, "-", show us, "  ", ut]

doelpunt' :: Bool -> State Stand String
doelpunt' thuisofuit = State (\(tt, ts, us, ut) ->
  let ts' = if thuisofuit then ts+1 else ts
      us' = if thuisofuit then us else us+1
      leider = if ts' > us' then tt else (if ts' < us' then ut else "Gelijkspel")
  in (leider, (tt, ts', us', ut)))

thuisDoelpunt' :: State Stand String
thuisDoelpunt' = doelpunt' True

uitDoelpunt' :: State Stand String
uitDoelpunt' = doelpunt' False

--fcg_psv' :: State Stand String
fcg_psv' = do uitDoelpunt'
              thuisDoelpunt'
              uitDoelpunt'
              uitDoelpunt'