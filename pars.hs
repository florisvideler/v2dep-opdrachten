import Control.Monad.Trans.State
import Control.Monad
import Control.Applicative

type Parser = (StateT String Maybe)

pChar :: Parser Char
pChar = do text <- get
           if length text > 0
             then modify tail >> return (head text)
             else mzero

parseOn = runStateT

pTwoChars :: Parser [Char]
pTwoChars = do firstChar <- pChar
               secondChar <- pChar
               return [firstChar, secondChar]

pString :: String -> Parser String
pString s = do firstChar <- pChar
             rest <- pString (tail s) <|> pEmpty
             return $ firstChar : rest

pEmpty :: Parser String
pEmpty = do text <- get
            if null text
              then return ""
              else mzero

pSpecificChar :: Char -> Parser Char
pSpecificChar sought = do parsed <- pChar
                          if parsed == sought
                            then return parsed
                            else mzero


-- <|> combineert twee parsers door beide te proberen