data Person = Person { name   :: String        -- Record syntax
                     , father :: Maybe Person  -- maakt automatisch functies:
                     , mother :: Maybe Person  -- father :: Person -> Maybe Person
                     } deriving Show                   -- zelfde voor name, mother

rickard = Person "Rickard" Nothing Nothing
ned = Person "Ned" (Just rickard) Nothing
robb = Person "Robb" (Just ned) Nothing

-- grandfather :: Person -> Maybe Person
-- grandfather p = father p >>= father
--
-- countParents :: Person -> Int
-- countParents (Person _ Nothing Nothing)   = 0
-- countParents (Person _ (Just _) (Just _)) = 2
-- countParents _                            = 1
--
-- cpgp :: Person -> Maybe Int
-- cpgp p = do f <- father p
--             return $ countParents f
--
-- cpgp' :: Person -> Maybe Int
-- cpgp' p = father p >>= (pure . countParents)
--
-- cgp :: Person -> Maybe Int
-- cgp p = do f <- father p
--            m <- mother p
--            pure $ (countParents f) + (countParents m)
--
-- cgp' :: Person -> Maybe Int
-- cgp' p = father p >>= \f -> mother p >>= \m -> pure $ (countParents f) + (countParents m)


{- In plaats van
[(+1), (*2)] <*> [3, 4, 5] -- [4, 5, 6, 6, 8, 10]

-- willen we dit:
[(+1), (*2)] <*> [3, 4, 5] -- [3+1, 4*2] -}

-- Gegeven
-- newtype ZipList a = ZipList [a] deriving Show
--
-- -- Geef de instanties voor Functor en Applicative:
-- instance Functor ZipList where
-- fmap f (ZipList zl) = ZipList $ f zl
--
-- instance Applicative ZipList where
-- (ZipList fs) <*> (ZipList vs) = ZipList $ zipWith ($) fs vs
-- pure a = ZipList [a]














